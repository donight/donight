(function() {
    'use strict';

    angular
        .module('donight.utils', [])
        .factory('Utils', Utils);

    /* @ngInject */
    function Utils() {
        function formatDateToString(date) {
            return date.format('YYYY-MM-DD');
        }

        return {
            formatDateToString: formatDateToString
        };
    }

})();
