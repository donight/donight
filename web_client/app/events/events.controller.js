(function() {
    'use strict';

    angular
        .module('donight.events', [])
        .controller('EventsController', EventsController)
        .filter('price', PriceDisplay);

    /* @ngInject */
    function EventsController(Event, $state, $scope, Utils) {
        var eventsVm = this;

        function updateEvents(filters) {
            var today = Utils.formatDateToString(moment());
            Event.query({
                date_between_0: filters.from || today,
                date_between_1: filters.to || today,
                ordering: 'start_time'
            }, function(events) {
                eventsVm.events = events;
            });
        }
        updateEvents($state.params);

        $scope.$watch(function() {return $state.params}, function() {
            updateEvents($state.params);
        });
    }

    /* @ngInject */
    function PriceDisplay() {
        var specialPriceDisplays = {
            '': 'לא ידוע',
            '0': 'חינם !'
        };

        return function(price) {
            var specialDisplay = specialPriceDisplays[price];
            return specialDisplay !== undefined ? specialDisplay : price + '₪';
        };
    }

})();
