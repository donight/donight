(function() {
    'use strict';

    angular
        .module('donight.events')
        .factory('Event', Event);

    /* @ngInject */
    function Event($resource) {
        return $resource('/api/events/',
            {date_between_0: '', date_between_1: ''},
            {
                query: {method: 'GET', isArray: true}
            }
        );
    }

})();
