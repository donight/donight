(function() {
    'use strict';

    angular
        .module('donight.filter')
        .controller('FilterController', FilterController)
        .filter('calendar', CalendarDay);

    /* @ngInject */
    function FilterController($state, Categories, Utils) {
        var filterVm = this;

        filterVm.week = _.map(_.range(7), function(diff) {
            return moment().add(diff, 'day');
        });

        filterVm.categories = Categories.query();

        filterVm.filterByDay = function(day) {
            var dayString = Utils.formatDateToString(day);
            $state.go('events.filter', {from: dayString, to: dayString});
        };

        filterVm.filterByCategory = function(category) {
            $state.go('events.filter', {eventTypes: [category.name]});
        };
    }

    /* @ngInject */
    function CalendarDay() {
        return function(day) {
            return day.calendar(moment(), {sameDay: 'הערב', nextDay: 'מחר', nextWeek: 'dddd'});
        };
    }

})();
