(function() {
    'use strict';

    angular
        .module('donight.filter', [])
        .factory('Categories', Categories);

    /* @ngInject */
    function Categories($resource) {
        return $resource('/api/categories/', {}, {
            query: {method: 'GET', isArray: true}
        });
    }

})();
