from django.contrib import admin
from donight.events.models import Event, Category, SubCategory


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'start_time', 'location', 'price')
    list_filter = ('start_time', 'title', 'location', 'price')
    ordering = ('start_time',)
    date_hierarchy = 'start_time'


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_name', 'position')


@admin.register(SubCategory)
class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'display_name', 'category', 'position')



