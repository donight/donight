import multiprocessing
import os
import subprocess

import django_filters
from config.common import BASE_DIR
from django.contrib.staticfiles.views import serve
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie

from donight.config import DEBUG
from donight.events.models import Event, Category, SubCategory
from donight.events.serializers import EventSerializer, CategorySerializer
from donight.authentication.permissions import IsAdminOrReadOnly
from event_finder import EventFinder

from rest_framework import mixins, viewsets, filters
from rest_framework.permissions import AllowAny


@ensure_csrf_cookie
def home(request):
    return serve(request, 'index.html', insecure=True)


def index_events(request):
    subprocess.Popen("python {}".format(os.path.join(BASE_DIR, "scripts", "index.py")), shell=True)
    return HttpResponse()


class EventFilter(filters.FilterSet):
    min_price = django_filters.NumberFilter(name="price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="price", lookup_expr='lte')
    date_between = django_filters.DateFromToRangeFilter(name="start_time")
    class Meta:
        model = Event
        fields = ['min_price', 'max_price', 'date_between']


class EventViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    """
    Creates, Updates, Retrieves and Lists Events.
    Allows filtering and ordering of the events.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAdminOrReadOnly, )

    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    filter_class = EventFilter
    ordering_fields = ('start_time', )


class CategoryViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    """
    Lists all categories with their sub categories.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (AllowAny, )

