from __future__ import unicode_literals

from django.db import models

MEDIUM_STR_LEN = 1024


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=MEDIUM_STR_LEN)
    display_name = models.CharField(max_length=MEDIUM_STR_LEN)
    position = models.IntegerField(unique=True)

    class Meta:
        verbose_name_plural = 'Categories'
        ordering = ('position', )

    def __unicode__(self):
        return self.display_name


class SubCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=MEDIUM_STR_LEN)
    display_name = models.CharField(max_length=MEDIUM_STR_LEN)
    category = models.ForeignKey(Category, related_name='sub_categories')
    position = models.IntegerField()

    class Meta:
        verbose_name_plural = 'Sub Categories'
        unique_together = (('category', 'position'), )
        ordering = ('category__position', 'position')

    def __unicode__(self):
        return '{} -> {}'.format(unicode(self.category), self.display_name)


class Event(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=MEDIUM_STR_LEN)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True, blank=True)
    location = models.CharField(max_length=MEDIUM_STR_LEN)
    price = models.CharField(max_length=MEDIUM_STR_LEN, blank=True)
    url = models.TextField(blank=True)
    description = models.TextField(blank=True)
    image = models.TextField(blank=True)

    # Facebook
    owner_name = models.TextField(null=True, blank=True)
    owner_id = models.BigIntegerField(null=True, blank=True)
    ticket_url = models.TextField(null=True, blank=True)

    # Categories
    categories = models.ManyToManyField(Category, related_name='events')
    sub_categories = models.ManyToManyField(SubCategory, related_name='events')

    def __repr__(self):
        return unicode(self)

    def __unicode__(self):
        return u'{0} ({1}) @ {2}'.format(self.title, self.location, self.start_time)


# class FacebookGroup(Base):
#     __tablename__ = 'facebook_groups'
#
#     owner_id = Column(Integer, primary_key=True)
#     owner_name = Column(String(MEDIUM_STR_LEN))
#     type_name = Column(String(MEDIUM_STR_LEN))
#
#     @classmethod
#     def get_group(cls, owner_id):
#         return session.query(cls).filter(cls.owner_id == owner_id).first()
#
#     @classmethod
#     def get_event_type(cls, owner_id):
#         group = cls.get_group(owner_id)
#         if group is None:
#             return Event
#         return get_event_type_by_type_name(group.type_name)
#
#     @classmethod
#     def create_if_doesnt_exist(cls, owner_id, owner_name):
#         group = cls.get_group(owner_id)
#         if group is None:
#             session.add(cls(owner_id=owner_id, owner_name=owner_name))
#             session.commit()
#
#     @property
#     def owner_url(self):
#         if self.owner_id:
#             return "https://www.facebook.com/" + str(self.owner_id)
#         return ''

