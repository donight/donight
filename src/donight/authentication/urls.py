from django.conf.urls import include, url
from rest_framework import urls as rest_urls

from rest_framework.urlpatterns import format_suffix_patterns

from donight.authentication import views

urlpatterns = [
    url(r'^login/$', views.Login.as_view()),
    url(r'^logout/$', views.Logout.as_view()),
    url(r'^active-user/$', views.ActiveUser.as_view()),
    url(r'^api-auth/', include(rest_urls, namespace='rest_framework')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
