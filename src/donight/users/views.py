from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny

from donight.users.models import User
from donight.authentication.permissions import IsOwnerOrReadOnly
from donight.users.serializers import UserSerializer


class UserViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """
    Creates, Updates, and retrives User accounts
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsOwnerOrReadOnly,)

    def get_permissions(self):
        if self.request.method == 'POST':
            return [AllowAny()]
        return [IsOwnerOrReadOnly()]


