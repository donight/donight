import datetime as dt
import re
import urlparse

from donight.event_finder.scrapers.base_scraper import Scraper
from donight.event_finder.scrapers.utils import get_soup, price_from_text_heuristic
from donight.event_finder.utils.general import to_local_timezone
from donight.events.models import Event


class BarbyScraper(Scraper):
    """
    This scraper is used to scrape music shows from the Barby website.
    """
    EVENTS_URL = r"https://www.barby.co.il/"

    LOCATION = u'\u05d1\u05d0\u05e8\u05d1\u05d9'

    TIME_REGEX = '(\d\d\/\d\d\/\d\d\d\d).*(\d\d:\d\d).*'
    TIME_FORMAT = '%d/%m/%Y %H:%M'

    def scrape(self):
        """
        Scrapes the Barby's website for all their events.
        :return: A list of all the events.
        :rtype: list(Event)
        """
        soup = get_soup(self.EVENTS_URL)
        event_elements = soup.find_all('td', {'class': 'defaultRowHeight'})
        todays_event = soup.find('div', {'class': 'contMailDivHazmen2'})
        event_urls = map(self.get_url, event_elements + [todays_event])
        return map(self.create_event_from_page_url, event_urls)

    def create_event_from_page_url(self, url):
        """
        Extracts the information in the events page and creates an Event.
        :return: An event containing all the information from the element.
        :rtype: donight.events.models.Event
        """
        event_page = get_soup(url)

        image = event_page.find('div', {'class': 'showCatRightDiv'}).find('img').get('src')
        title = event_page.find('div', {'class': 'showCatLeftDivImg'}).find('h1').text.strip()
        description = self.get_description(event_page)
        start_time = self.get_start_time(event_page)
        price_element = event_page.find('span', {'class': 'showCatlbPrice'})
        price = price_from_text_heuristic(price_element.text)

        return Event(title=title, start_time=start_time, location=self.LOCATION,
                     price=price, url=url, description=description, image=image)

    def get_url(self, event_element):
        """
        Extracts the url from a barby event element.
        :rtype: str
        """
        return urlparse.urljoin(self.EVENTS_URL, event_element.find('a').get('href'))

    def get_description(self, event_page):
        """
        Extracts the description.
        """
        return '\n\n'.join([paragraph.text.strip() for paragraph in
                            event_page.find('span', {'class': 'showCatlblDesc'}).children
                            if hasattr(paragraph, 'text') and paragraph.text.strip()])

    def get_start_time(self, event_page):
        """
        Extracts the start time from the event page.
        """
        time_text = event_page.find_all('span', {'class': 'showCatlblDate'})[1].text
        date, time = re.match(self.TIME_REGEX, time_text).groups()
        start_time_string = "{} {}".format(date, time)
        start_time = to_local_timezone(dt.datetime.strptime(start_time_string, self.TIME_FORMAT))
        return start_time

