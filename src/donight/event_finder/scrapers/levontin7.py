import datetime as dt
import json
import time

import requests
from donight.event_finder.scrapers.base_scraper import Scraper
from donight.event_finder.utils.general import SECONDS_IN_MONTH, SECONDS_IN_YEAR, to_local_timezone, find
from donight.events.models import Event
from donight.event_finder.scrapers.utils import price_from_text_heuristic


class Levontin7Scraper(Scraper):
    """
    This scraper is used to scrape indie music shows from the Levontin 7 website.
    """
    URL = 'http://www.levontin7.com/'

    EVENTS_PARAMS = {'rhc_action': 'get_calendar_events',
                     'post_type': 'events'}

    LEVONTIN_LOCATION = u'\u05dc\u05d1\u05d5\u05e0\u05d8\u05d9\u05df 7'

    TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    def scrape(self):
        """
        This method scrapes events (music shows) from the Levontin 7 website.
        Levontin's website exposes a restful json api for its clients,
        so we use that api and just parse the json for the events.
        :return: A list of all the events in Levontin 7.
        :rtype: list(Event)
        """
        event_range = {'start': time.time() - 3 * SECONDS_IN_MONTH, 'end': time.time() + SECONDS_IN_YEAR}
        response = requests.post(self.URL, params=self.EVENTS_PARAMS, data=event_range)

        events_list = json.loads(response.content)['EVENTS']
        return map(self.levontin_json_to_event, events_list)

    def levontin_json_to_event(self, levontin_event):
        """
        Receives a json dict from the levontin restful api representing an event,
        extracts as much data as it can from the dict, and returns an Event object with the data.
        :param levontin_event: A dict representing an event from the Levontin api.
        :type levontin_event: dict
        :return: An event containing the information from the levontin event.
        :rtype: donight.events.models.Event
        """
        try:
            start_time = self.levontin_time_to_time(levontin_event['start'])
            end_time = self.levontin_time_to_time(levontin_event['end'])
            description = self.remove_line_breaks_from_description(levontin_event['description'])
            price = price_from_text_heuristic(description)
            image = levontin_event['image'][0] if levontin_event['image'] else ''
            return Event(title=levontin_event['title'],
                         start_time=start_time,
                         end_time=end_time,
                         location=self.LEVONTIN_LOCATION,
                         price=price,
                         url=levontin_event['url'],
                         description=description,
                         image=image)
        except Exception:
            self.logger.exception("Failed turning a Levontin event into an event, "
                                  "the Levontin event is: \n%s\nException:", json.dumps(levontin_event, indent=4))
            return None

    def remove_line_breaks_from_description(self, description):
        """
        The way that the Levontin website works is that the description received from the json api
        is rendered as html.
        We only need the text from the description, and don't want to bloat it with html line breaks,
        so we remove those.
        :param description: The description of an event from the Levontin website.
        :type description: str
        :return: The same description, without html line breaks.
        :rtype: str
        """
        # TODO: remove all html elements, leave only text.
        return description.replace('<br>\n', '').replace('<br>', '')

    def levontin_time_to_time(self, levontin_time):
        """
        Creates a datetime object from a string describing time in a levontin event.
        :param levontin_time: The time string from a levontin event.
        :type levontin_time: str
        :return: A datetime object of that time.
        :rtype: datetime.datetime
        """
        return to_local_timezone(dt.datetime.strptime(levontin_time, self.TIME_FORMAT))

