import datetime as dt

from donight.event_finder.scrapers.base_scraper import Scraper
from donight.event_finder.scrapers.utils import get_soup
from donight.events.models import Event
from donight.event_finder.utils.general import to_local_timezone


class Rothschild12Scraper(Scraper):
    """
    This scraper is used to scrape music shows from the Rothschild 12 website.
    """
    EVENTS_URL = "http://www.rothschild12.co.il/author/r12/"

    LOCATION = u'\u05e8\u05d5\u05d8\u05e9\u05d9\u05dc\u05d3 12'

    TIME_FORMAT = '%d/%m/%Y | %H:%M'

    FAKE_EVENT_NAMES = [
        u'\u05d0\u05d9\u05df \u05d4\u05d5\u05e4\u05e2\u05d4',
        u'Place closed'
    ]

    def scrape(self):
        """
        Scrapes events off the Rothschild 12.
        We use a wordpress view to get all posts.
        They currently only post a day or two ahead,
        so we only parse the following 9 events.
        :return: A list of all the events in Rothschild 12.
        :rtype: list(donight.models.Event)
        """
        events_page = get_soup(self.EVENTS_URL)
        event_elements = events_page.find_all('article')
        events = map(self.create_event_from_element, event_elements)
        return filter(self.is_real_event, events)

    def create_event_from_element(self, event_element):
        """
        Scrapes out the important data from the html,
        and returns an Event object representing the event.
        :param event_element: A beutifulsoup element representing an event.
        :type event_element: bs4.Tag
        :return: An event containing all the information from the element.
        :rtype: donight.events.models.Event
        """
        url = event_element.find('a').get('href')
        event_soup = get_soup(url)

        content = event_soup.find('div', {'class': 'content'})
        title = content.find('h1').text
        time_element = content.find('h4')
        if time_element is None:
            return None
        start_time = to_local_timezone(dt.datetime.strptime(
            time_element.text, self.TIME_FORMAT))
        price = 0 # All events in Rothschild 12 are free :)
        description = '\n\n'.join(description_element.text
                                 for description_element in content.find_all('p')
                                  if description_element.text.strip())
        image = event_element.find('img').get('src', '')

        return Event(title=title, start_time=start_time, location=self.LOCATION,
                     price=price, url=url, description=description, image=image)

    def is_real_event(self, event):
        """
        Their site has many placeholder events for days without events.
        :param event: The event that could be fake.
        :type event: donight.models.Event
        :return: Whether the event is real or not.
        :rtype: bool
        """
        return (event is not None and
                all(fake_name not in event.title for fake_name in self.FAKE_EVENT_NAMES))


