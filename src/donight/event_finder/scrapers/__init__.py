from donight.event_finder.scrapers.base_scraper import Scraper
from donight.event_finder.scrapers.levontin7 import Levontin7Scraper
from donight.event_finder.scrapers.ozen_bar import OzenBarScraper
from donight.event_finder.scrapers.shows_around import ShowsAroundScraper
from donight.event_finder.scrapers.rothschild12 import Rothschild12Scraper
from donight.event_finder.scrapers.tsuzamen import TsuzamenScraper
from donight.event_finder.scrapers.barby import BarbyScraper


def get_all_scrapers():
    return [
        Levontin7Scraper(),
        OzenBarScraper(),
        ShowsAroundScraper(),
        Rothschild12Scraper(),
        TsuzamenScraper(),
        BarbyScraper()
    ]
