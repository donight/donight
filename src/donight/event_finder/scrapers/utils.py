import re

import requests
from bs4 import BeautifulSoup
from donight.event_finder.utils.general import find

CURRENCY_STRINGS = [u'\u20aa', u'\u05e9\u05e7\u05dc', u'\u05e9\u05d7', u'\u05e9"\u05d7']

FREE_STRINGS = [u'\u05d7\u05d9\u05e0\u05dd',
                u'\u05db\u05e0\u05d9\u05e1\u05d4 \u05d7\u05d5\u05e4\u05e9\u05d9\u05ea']

URL_REGEX = re.compile('((?:(https?|s?ftp):\\/\\/)?(?:www\\.)?((?:(?:[A-Z0-9][A-Z0-9-]{0,61}[A-Z0-9]\\.)+)([A-Z]{2,6})|(?:\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}))(?::(\\d{1,5}))?(?:(\\/\\S+)*))', re.IGNORECASE)

def price_from_text_heuristic(text, currency_strings=CURRENCY_STRINGS, free_strings=FREE_STRINGS):
    """
    This function uses heuristics on the description of events to try to find the price of an event.
    It works in about 95% of the events. (it doesn't work when the price isn't included in the description).
    :param text: The text to find the price from.
    :type text: str
    :param currency_strings: Strings that represent a price.
    :type currency_strings: list(str)
    :param free_strings: Strings that indicate the event is free.
    :type free_strings: list(str)
    :return: The price if it could be deducted.
    :rtype: str
    """
    # TODO: refactor !
    if any([free_string in text for free_string in free_strings]):
        return '0'

    for currency in currency_strings:
        if currency in text:
            text_words = text.split()

            if currency in text_words:
                # There is a space between the currency and the price, we take the price that is before the currency.
                shekel_index = text_words.index(currency)
                return text_words[shekel_index - 1]
            else:
                # There is no space between the currency and the price, so we take the price from the word of the currency.
                shekel_word = find(text_words, lambda word: currency in word, '')
                return shekel_word.replace(currency, '')

    return ''

def find_urls(string):
    """
    Finds urls in a string.
    Returns the url and the domain name.
    :param string: The string to search for urls.
    :type string: str
    :return: A list of the urls and domain names.
    :rtype: list(tuple(str, str))
    """
    return [(url[0], url[2]) for url in URL_REGEX.findall(string)]


def get_soup(url):
    response = requests.get(url)
    return BeautifulSoup(response.text, 'lxml')

