import datetime as dt

from donight.event_finder.scrapers.base_scraper import Scraper
from donight.events.models import Event
from donight.event_finder.utils.general import to_local_timezone
from donight.event_finder.scrapers.utils import price_from_text_heuristic, find_urls, get_soup


class TsuzamenScraper(Scraper):
    """
    This scraper is used to scrape music shows from the Tsuzamen Bar website.
    """
    EVENTS_URL = "https://tsuzamenbar.com/events/"

    LOCATION = u'\u05e6\u05d5\u05d6\u05d0\u05de\u05df'

    DATE_FORMAT = '%d/%m/%Y'
    TIME_FORMAT = '{} %H:%M'.format(DATE_FORMAT)

    FAKE_EVENT_NAMES = [
        u'\u05e7"\u05e7',
        u'\u05d1\u05de\u05d4 \u05e4\u05ea\u05d5\u05d7\u05d4',
        u'\u05e1\u05d2\u05d5\u05e8'
    ]

    NON_EVENT_DOMAINS = ['youtube']

    def __init__(self, logger=None):
        super(TsuzamenScraper, self).__init__(logger=None)

    def scrape(self):
        """
        Scrapes events off the Tsuzamen.
        They have a table containing the event info.
        :return: A list of all the events.
        :rtype: list(donight.models.Event)
        """
        events_page = get_soup(self.EVENTS_URL)
        article = events_page.find('article')
        event_elements = article.find_all('tr')[1:-1]

        # TODO: improve
        last_event_date = None
        events = []
        for event_element in event_elements:
            event = self.create_event_from_element(event_element, last_event_date)
            events.append(event)
            last_event_date = event.start_time

        return filter(self.is_real_event, events)

    def create_event_from_element(self, event_element, last_event_date):
        """
        Scrapes out the important data from the html,
        and returns an Event object representing the event.
        :param event_element: A beutifulsoup element representing an event.
        :type event_element: bs4.Tag
        :param last_event_date: The date of the event before this one.
        :type last_event_date: dt.datetime | None
        :return: An event containing all the information from the element.
        :rtype: donight.events.models.Event
        """
        (date_element, _, time_element, description_element,
             details_element) = tuple(event_element.find_all('td'))

        title, description = self.split_title(description_element)
        description = '\n'.join([desc for desc in (description, details_element.text) if desc.strip()])

        start_time = self.get_start_time(date_element, time_element, last_event_date)
        price = price_from_text_heuristic(description)
        url = self.get_url(description)
        image = ''

        return Event(title=title, start_time=start_time, location=self.LOCATION,
                     price=price, url=url, description=description, image=image)

    def split_title(self, description_element):
        """
        The tsuzamen have a description field containing
        the title and some description.
        We run a heuristic to split them.
        :param description_element: The element containing the description.
        :return: The title and the description split.
        :rtype: tuple(str, str)
        """
        for delimiter in (u'|', u'\u2013', u'-'):
            if delimiter in description_element.text:
                split_desc = map(unicode.strip, description_element.text.split(delimiter))
                return split_desc[0], delimiter.join(split_desc[1:-1])
        return (description_element.text.strip(), '')

    def get_start_time(self, date_element, time_element, last_event_date):
        """
        Not all events have either date or time.
        If an event doesn't have a date - it's the date of the event before it.
        If an event doesn't have a time - we assume it's 4 pm.
        """
        date_text = (date_element.text if date_element.text.strip()
                     else last_event_date.strftime(self.DATE_FORMAT))
        time_text = time_element.text if time_element.text.strip() else '16:00'
        date_time_text = "{} {}".format(date_text, time_text)
        return to_local_timezone(dt.datetime.strptime(date_time_text, self.TIME_FORMAT))

    def get_url(self, description):
        event_urls = [url for url, domain in find_urls(description)
                            if all(bad_domain not in domain for bad_domain in self.NON_EVENT_DOMAINS)]
        if not event_urls:
            return self.EVENTS_URL
        return event_urls[0]

    def is_real_event(self, event):
        """
        Their site has many recurring events that we don't care about.
        :param event: The event that could be fake.
        :type event: donight.models.Event
        :return: Whether the event is real or not.
        :rtype: bool
        """
        return (event is not None and
                all(not fake_name in event.title and not fake_name in event.description
                    for fake_name in self.FAKE_EVENT_NAMES))

