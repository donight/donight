import os
from donight.config.common import Common, BASE_DIR
from configurations import values


class Local(Common):
    INSTALLED_APPS = Common.INSTALLED_APPS
    INSTALLED_APPS += ("crispy_forms", )

    DEBUG = values.BooleanValue(True)
    for template_config in Common.TEMPLATES:
        template_config['OPTIONS']['debug'] = DEBUG

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
        # 'default': {
        #     'ENGINE': 'django.db.backends.postgresql_psycopg2',
        #     'NAME': 'd56i20arr9kudc',
        #     'USER': 'qxpmctjoxgdlqp',
        #     'PASSWORD': 'f010c0043c61cef8a24e4796d686c64593b6ba86c51fc872a867df845e2aafcf',
        #     'HOST': 'ec2-46-137-117-43.eu-west-1.compute.amazonaws.com',
        #     'PORT': '5432',
        # }
    }

    # Mail
    EMAIL_HOST = 'localhost'
    EMAIL_PORT = 1025
    EMAIL_BACKEND = values.Value('django.core.mail.backends.console.EmailBackend')

