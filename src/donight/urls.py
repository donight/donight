from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from donight.authentication.urls import urlpatterns as auth_urls
from donight.events.views import home, EventViewSet, CategoryViewSet
from donight.users.views import UserViewSet
from events.views import index_events
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'events', EventViewSet)
router.register(r'categories', CategoryViewSet)

urlpatterns = ([
                   url(r'^$', home),

                   url(r'^admin/', include(admin.site.urls)),

                   url(r'^api/', include(auth_urls)),
                   url(r'^api/', include(router.urls)),
                   url(r'^api/index$', index_events),
               ]
               + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
               + [url(r'^.*$', home)])
