import moment from 'moment';

export function formatDateToString(date: moment.Moment): string {
  return date.format('YYYY-MM-DD');
}
