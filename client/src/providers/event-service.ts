import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { EventData } from "../models/event-data";
import { Filters } from "../models/filters";
import { formatDateToString } from "./utils";
import { URLSearchParams } from "@angular/http";

@Injectable()
export class EventService {
  static url = '/api/events/';

  constructor(public http: Http) { }

  query(filters: Filters) : Promise<EventData[]> {
    let filterParams = new URLSearchParams();
    filterParams.set('date_between_0', formatDateToString(filters.from));
    filterParams.set('date_between_1', formatDateToString(filters.to));
    filterParams.set('ordering', 'start_time');
    return this.http.get(EventService.url, {search: filterParams})
      .toPromise()
      .then(EventService.extractData);
  }

  static extractData(response: Response): EventData[] {
    return response.json().map(EventData.fromJson);
  }

}
