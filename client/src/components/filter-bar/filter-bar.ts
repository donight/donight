import { Component, Output, EventEmitter } from '@angular/core';

import { PopoverController } from 'ionic-angular';
import { DateFilterPopover } from "../date-filter-popover/date-filter-popover";
import { Filters } from "../../models/filters";
import { Input } from "@angular/core";

@Component({
  selector: 'filter-bar',
  templateUrl: 'filter-bar.html'
})
export class FilterBar {
  @Output() onFilter: EventEmitter<Filters>;
  @Input() filters: Filters;

  constructor(public popoverCtrl: PopoverController) {
    this.onFilter = new EventEmitter();
  }

  filterDate(clickEvent) {
    let popover = this.popoverCtrl.create(DateFilterPopover);
    popover.onDidDismiss((filters: Filters) => this.onFilter.emit(filters));
    popover.present({ev: clickEvent})
  }

}
