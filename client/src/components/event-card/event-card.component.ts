import { Component, Input } from '@angular/core';

import { EventData } from "../../models/event-data";

@Component({
  selector: 'event-card',
  templateUrl: 'event-card.html'
})
export class EventCard {
  @Input() event: EventData;
  shouldExpand: boolean = false;

  expand() {
    this.shouldExpand = !this.shouldExpand;
  }
}
