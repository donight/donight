import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment'

import linkifyStr from 'linkifyjs/string';

@Pipe({name: 'calendar'})
export class CalendarPipe implements PipeTransform {
  private moment_formats = {
    'sameElse': 'MM/DD/YYYY בשעה HH:mm',
  };

  transform(date: moment.Moment): string {
    return date.calendar(null, this.moment_formats);
  }
}

@Pipe({name: 'price'})
export class PricePipe implements PipeTransform {
  static specialPriceDisplays = {
    '': 'לא ידוע',
    '0': 'חינם !'
  };

  transform(price: string): string {
    let specialDisplay = PricePipe.specialPriceDisplays[price];
    return specialDisplay !== undefined ? specialDisplay : price + '₪';
  }
}

@Pipe({name: 'linkify'})
export class LinkifyPipe implements PipeTransform {
  transform(str: string): string {
    return str ? linkifyStr(str, {target: '_blank', className: 'linkify'}) : str;
  }
}

