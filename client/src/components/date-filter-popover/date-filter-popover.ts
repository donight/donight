import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { Filters } from "../../models/filters";

@Component({
  selector: 'date-filter-popover',
  templateUrl: 'date-filter-popover.html'
})
export class DateFilterPopover {
  dateFilters: Filters[];

  constructor(public viewCtrl: ViewController) {
    this.dateFilters = Filters.createDateFilters();
  }

  choose(filters: Filters) {
      this.viewCtrl.dismiss(filters);
  }
}
