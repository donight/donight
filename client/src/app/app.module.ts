import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Donight } from './app.component';
import { EventsPage } from '../pages/events-page/events-page';
import { EventCard } from "../components/event-card/event-card.component";
import { CalendarPipe, PricePipe, LinkifyPipe } from "../components/event-card/event-card.pipes";
import { EventService } from "../providers/event-service";
import { FilterBar } from "../components/filter-bar/filter-bar";
import { DateFilterPopover } from "../components/date-filter-popover/date-filter-popover";

@NgModule({
  declarations: [
    Donight,
    EventCard,
    EventsPage,
    CalendarPipe,
    PricePipe,
    LinkifyPipe,
    DateFilterPopover,
    FilterBar
  ],
  imports: [
    IonicModule.forRoot(Donight),
    FormsModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Donight,
    EventsPage,
    DateFilterPopover
  ],
  providers: [EventService]
})
export class AppModule {}
