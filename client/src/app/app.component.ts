import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import moment from 'moment';
import 'moment/src/locale/he';

import { EventsPage } from '../pages/events-page/events-page';


@Component({
  templateUrl: 'app.html'
})
export class Donight {
  rootPage = EventsPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      moment.locale('he');

    });
  }
}
