import { Component } from '@angular/core';

import _ from 'lodash'

import { EventData } from "../../models/event-data";
import { EventService } from "../../providers/event-service";
import { Filters } from "../../models/filters";
import { PopoverController } from "ionic-angular";
import { DateFilterPopover } from "../../components/date-filter-popover/date-filter-popover";

@Component({
  selector: 'events-page',
  templateUrl: 'events-page.html'
})
export class EventsPage {
  events: EventData[];
  filters: Filters;

  constructor(private eventService: EventService, public popoverCtrl: PopoverController) {
    this.filters = Filters.createDateFilters()[0];
  }

  ngOnInit(): void {
    this.updateEvents(this.filters);
  }

  updateFilters(newFilters: Filters): void {
    let getOrigOnUndefined = (origValue, newValue) => _.isUndefined(newValue) ? origValue : newValue;
    this.filters = _.extendWith(this.filters, newFilters, getOrigOnUndefined);
    this.updateEvents(this.filters);
  }

  updateEvents(filters: Filters): void {
    this.eventService.query(filters)
      .then(events => this.events = events)
      // Todo: handle errors correctly.
      .catch(message => console.log('we should handle errors !'));
  }

  filterDate(clickEvent) {
    let popover = this.popoverCtrl.create(DateFilterPopover);
    popover.onDidDismiss((filters: Filters) => this.updateFilters(filters));
    popover.present({ev: clickEvent})
  }

}
