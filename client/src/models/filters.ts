import moment from 'moment';

export class Filters {
  static THURSDAY = 4;
  static SATURDAY = 6;

  constructor(
    public from?: moment.Moment,
    public to?: moment.Moment,
    public dateRepr?: string
  ) {}

  static getDayOfWeek(day : number) : moment.Moment {
    return moment.max(moment(), moment().day(day));
  }

  static createDateFilters(): Filters[] {
    return [
      new Filters(moment(), moment(), 'היום'),
      new Filters(moment().add(1, 'day'), moment().add(1, 'day'), 'מחר'),
      new Filters(this.getDayOfWeek(Filters.THURSDAY), this.getDayOfWeek(Filters.SATURDAY), 'סוף השבוע'),
      new Filters(moment(), moment().add(7, 'day'), 'השבוע הקרוב'),
    ]
  }
}

