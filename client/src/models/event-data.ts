import moment from 'moment';

export class EventData {
  constructor(
    public title: string,
    public description: string,
    public startTime: moment.Moment,
    public location: string,
    public price?: string,
    public url?: string,
    public image?: string,
    public endTime?: moment.Moment,
    public id?: number,
  ) {}

  static fromJson(eventJson: any): EventData {
    return new EventData(
      eventJson.title,
      eventJson.description,
      moment(eventJson.start_time),
      eventJson.location,
      eventJson.price,
      eventJson.url,
      eventJson.image,
      eventJson.end_time ? moment(eventJson.end_time) : eventJson.end_time,
      eventJson.id)
  }
}

